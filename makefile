GCC= g++

all: output

menu.o: menu.cpp
	 $(GCC) -c  menu.cpp 

mapgen.o: mapgen.cpp mapgen.h
	 $(GCC) -c  mapgen.cpp 

output: menu.o mapgen.o
	$(GCC) menu.o mapgen.o -Wall -Wextra -Wdouble-promotion -o roguelike  -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio

make clean:
	rm -r *.o
	rm -f roguelike
