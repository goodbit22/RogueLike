# RogueLike

A simple roguelike game

![project](./img/graphic.png)

## Table of Contents

* [Running and installation](#runningandinstallation)
* [Technologies](#technologies)
* [Authors](#authors)

## Running and installation

### Prerequisites

To run RogueLike you will need to install **SFML library**

### Compilation

To compile the project you just need to type command in the terminal

```sh
make
```

### Running

```sh
./roguelike
```

## Technologies

Project is created with:

* C++11
* STL
* SFML 2.4

## Authors

*****
**goodbit22** --> <https://gitlab.com/users/goodbit22>
*****
**hagaven**   --> <https://gitlab.com/hagaven>
*****
