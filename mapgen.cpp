#include "mapgen.h"

	Tile::Tile(int x,int y){
		empty=false;
		wall=false;
		this->x=x;
		this->y=y;
	}
	void Tile::occupy(){
		this->empty=false;
	}
	void Tile::leave(){
		this->empty=true;
	}
	void Tile::setwall(){
		this->empty=false;
	}
	void Tile::removewall(){
		this->empty=true;
	}

	bool Tile::iswall(){
	return wall;
	}
	bool Tile::isempty(){
		return empty;
	}
	int Tile::getx(){
		return x;
	}

	int Tile::gety(){
		return y;
	}

	bool Tile::zapisz(ofstream *plik){
	plik->open("mapa_save.txt", ios::out | ios::app);
    if(plik->is_open()){
        (*plik)<<x<<" "<<y<<" "<<empty<<" "<<wall<<" ";
        plik->close();
        return true;
    }
    else
        return false;
	}

	bool Tile::wczytaj(ifstream *plik){
	if(plik->is_open()){
        if(!plik->eof()){
            (*plik)>>x>>y>>empty>>wall;
            return true;
        }
        else{
            cout<<"\nkoniec\n";
            plik->close();
            return false;
        }
    }
    else{
        cout<<"\nblad otwarcia\n";
        return false;
    }
	}


class Room{
public:
	int x1,x2,y1,y2,w,h;
	Room(int maxw, int maxh,int *seed){
		int x,y,w,h;
        srand(*seed);
        *seed=rand();
		x=(*seed)%(maxw-20);
		srand(*seed);
		*seed=rand();
		y=(*seed)%(maxh-20);
		srand(*seed);
		*seed=rand();
		w=10+(*seed)%10;
		srand(*seed);
		*seed=rand();
		h=10+(*seed)%10;
		srand(*seed);
		*seed=rand();
		x1=x;
		x2=x+w;
		y1=y;
		y2=y+h;
		this->w=w;
		this->h=h;
	}
	bool intersect(Room room){
		if (x1<=room.x2 && x2>=room.x1 && y1<=room.y2 && y2>=room.y1)
		return true;
		else
		return false;
	}

};


class Przedmiot{
    double maxhp ,hp, atak, unik, obrona, szybkosc;
    int x,y;
    public:
    sf::Sprite tekstura;
    sf::Text nazwa;
    public:
        Przedmiot(vector<Tile> &grid,Room pokoj,int *seed,sf::Font font,sf::Texture *texture){
        srand(*seed);
        maxhp=rand()%12-2;
        *seed=rand();
        srand(*seed);
        hp=rand()%50-2;
        *seed=rand();
        srand(*seed);
        atak=rand()%12-2;
        *seed=rand();
        srand(*seed);
        unik=rand()%12-2;
        *seed=rand();
        srand(*seed);
        obrona=rand()%12-2;
        *seed=rand();
        srand(*seed);
        szybkosc=rand()%3-2;
        *seed=rand();
        srand(*seed);


        this->tekstura.setTextureRect(sf::IntRect(0,0,10,10));
        this->tekstura.setTexture(*texture);
        this->tekstura.setScale(4,4);
        int tempx=10,tempy=10;
        srand(*seed);
        int koord=0;
        do{
            tempx=pokoj.x1+1+rand()%(pokoj.w-2);
            *seed=rand();
            srand(*seed);
            tempy=pokoj.y1+1+rand()%(pokoj.h-2);
            *seed=rand();
            srand(*seed);
            koord=tempx+100*tempy;
        }while(!grid.at(koord).isempty());
        this->x=tempx;
        this->y=tempy;
        stringstream tmp;
        string tmp2,tmp3;
        tmp<<hp<<' '<<atak<<' '<<obrona<<' '<<unik<<' '<<' '<<szybkosc;
        tmp>>tmp2>>tmp3;
        tmp2="Mikstura\nhp:"+tmp2+"\natak:"+tmp3;
        tmp>>tmp3;
        tmp2=tmp2+"\nobrona:"+tmp3;
        tmp>>tmp3;
        tmp2=tmp2+"\nunik:"+tmp3;
        tmp>>tmp3;
        tmp2=tmp2+"\nszybkosc:"+tmp3;

        nazwa.setString(tmp2);
        nazwa.setFont(font);
        nazwa.setPosition(850,400);
        }
    Przedmiot(sf::Font font,sf::Texture *texture){
        maxhp=hp=atak=obrona=unik=szybkosc=0;
        x=y=0;
        this->tekstura.setTextureRect(sf::IntRect(0,0,10,10));
        this->tekstura.setTexture(*texture);
        this->tekstura.setScale(4,4);
        this->nazwa.setFont(font);
        this->nazwa.setPosition(850,400);
        }

    int getx(){
    return x;
    }

    int gety(){
    return y;
    }

    double getmaxhp(){
    return maxhp;
    }

    double gethp(){
    return hp;
    }

    double getatak(){
    return atak;
    }

    double getunik(){
    return unik;
    }

    double getobrona(){
    return obrona;
    }

    double getszybkosc(){
    return szybkosc;
    }

    void setx(int x){
    this->x=x;
    }

    void sety(int y){
    this->y=y;
    }

    bool zapisz(ofstream *plik){
    plik->open("przedmioty_save.txt", ios::out | ios::app);
    if(plik->is_open()){
        (*plik)<<maxhp<<" "<<hp<<" "<<atak<<" "<<unik<<" "<<obrona<<" "<<szybkosc<<" "<<x<<" "<<y<<" ";
        plik->close();
        return true;
    }
    else
        plik->close();
        return false;
    }

    bool wczytaj(ifstream *plik,sf::Font font){
    if(plik->is_open()){
        if(!plik->eof()){
            (*plik)>>maxhp>>hp>>atak>>unik>>obrona>>szybkosc>>x>>y;
            stringstream tmp;
            string tmp2,tmp3;
            tmp<<hp<<' '<<atak<<' '<<obrona<<' '<<unik<<' '<<' '<<szybkosc;
            tmp>>tmp2>>tmp3;
            tmp2="Mikstura\nhp:"+tmp2+"\natak:"+tmp3;
            tmp>>tmp3;
            tmp2=tmp2+"\nobrona:"+tmp3;
            tmp>>tmp3;
            tmp2=tmp2+"\nunik:"+tmp3;
            tmp>>tmp3;
            tmp2=tmp2+"\nszybkosc:"+tmp3;

            nazwa.setString(tmp2);
            nazwa.setFont(font);
            nazwa.setPosition(850,400);
            return true;
        }
        else{
            cout<<"\nkoniec\n";
            plik->close();
            return false;
        }
    }
    else{
        cout<<"\nblad otwarcia\n";
        return false;
    }
    }

    void showstat(){
    cout<<endl<<maxhp<<":"<<hp<<":"<<atak<<":"<<unik<<":"<<obrona<<":"<<szybkosc<<":"<<x<<":"<<y<<endl;
    }
};

class Obiekt{
    double maxhp, hp, atak, unik, obrona, szybkosc, tik;
    int x,y;
    vector<Przedmiot> eq;
public:
    sf::Sprite tekstura;
    sf::Text statystyka;
public:
    Obiekt(vector<Tile> &grid,Room pokoj,int *seed,sf::Texture *texture,sf::Font font){
    this->tekstura.setTextureRect(sf::IntRect(0,0,10,10));
    this->tekstura.setTexture(*texture);
    this->tekstura.setScale(4,4);
    int tempx=10,tempy=10;
    srand(*seed);
    int koord=0;
    do{
        tempx=pokoj.x1+1+rand()%(pokoj.w-2);
        *seed=rand();
        srand(*seed);
        tempy=pokoj.y1+1+rand()%(pokoj.h-2);
        *seed=rand();
        srand(*seed);
        koord=tempx+100*tempy;

    }while(!grid.at(koord).isempty());
    grid.at(koord).occupy();
    this->x=tempx;
    this->y=tempy;
    this->tekstura.setPosition(this->x*10,this->y*10);
    this->hp=50+rand()%80;
    this->maxhp=this->hp;
    *seed=rand();
    srand(*seed);
    this->atak=1+rand()%20;
    *seed=rand();
    srand(*seed);
    this->obrona=1+rand()%15;
    *seed=rand();
    srand(*seed);
    this->unik=1+rand()%2;
    *seed=rand();
    srand(*seed);
    this->szybkosc=30+rand()%10;
    *seed=rand();
    srand(*seed);
    this->tik=this->szybkosc;

    stringstream tmp;
        string tmp2,tmp3;
        tmp<<maxhp<<' '<<atak<<' '<<obrona<<' '<<unik<<' '<<' '<<szybkosc;
        tmp>>tmp2>>tmp3;
        tmp2="Statystyka\nhp:"+tmp2+"\natak:"+tmp3;
        tmp>>tmp3;
        tmp2=tmp2+"\nobrona:"+tmp3;
        tmp>>tmp3;
        tmp2=tmp2+"\nunik:"+tmp3;
        tmp>>tmp3;
        tmp2=tmp2+"\nszybkosc:"+tmp3;

        statystyka.setString(tmp2);
        statystyka.setFont(font);
        statystyka.setPosition(850,200);
    }
    Obiekt(sf::Font font,sf::Texture* texture){
        this->tekstura.setTextureRect(sf::IntRect(0,0,10,10));
        this->tekstura.setTexture(*texture);
        this->tekstura.setScale(4,4);
        statystyka.setFont(font);
        statystyka.setPosition(850,200);
        maxhp=hp=atak=unik=obrona=szybkosc=tik=0;
        x=y=0;
    }
    int getx(){
    return x;
    }

    int gety(){
    return y;
    }

    void setx(int x){
    this->x=x;
    }

    void sety(int y){
    this->y=y;
    }

    double getmaxhp(){
        return maxhp;
    }

    double gethp(){
        return hp;
    }

    double getatak(){
        return atak;
    }

    vector<Przedmiot> geteq(){
        return eq;
    }

    void dropeq(vector<Przedmiot> *przedmioty,sf::Texture *orb){

        Przedmiot tmp=eq.back();
        eq.pop_back();
        tmp.setx(this->x);
        tmp.sety(this->y);
        tmp.tekstura.setTexture(*orb);
        tmp.tekstura.setScale(4,4);
        przedmioty->insert(przedmioty->begin(),tmp);

    }

    void takedmg(double dmg){
        srand(time(0));
        int szansa=rand()%21;
        double obrazenia=dmg-obrona*(21-szansa)/20;
        if(obrazenia<0)
            obrazenia=1;
        if(szansa>unik)
            this->hp=this->hp-obrazenia;
    }

    void atakuj(Obiekt &przeciwnik){
        przeciwnik.takedmg(this->atak);
    }

    void kontaktdmg(Obiekt &przeciwnik){
        przeciwnik.takedmg(this->atak);
        takedmg(przeciwnik.getatak());
    }

    void atakowanie(int kierunek,vector<Tile> &grid,vector<Obiekt> *wrogowie,vector<Przedmiot> *przedmioty,sf::Texture *orb){
        switch (kierunek){
    case 1:
        for(vector<Obiekt>::iterator it=wrogowie->begin();it!=wrogowie->end();it++){
        if(it->getx()==(this->getx()+1)&&((it->gety()==(this->gety()-1))||(it->gety()==this->gety())||(it->gety()==(this->gety()+1)))){
                                          this->atakuj(*it);
                                          cout<<(*it).hp<<endl;
                                          if((*it).gethp()<0){
                                          grid.at((it->getx()+100*it->gety())).leave();
                                          it->dropeq(przedmioty,orb);
                                          wrogowie->erase(it);
                                          }
            };
            if(wrogowie->empty())
                break;
        }
        break;
    case 2:
        for(vector<Obiekt>::iterator it=wrogowie->begin();it!=wrogowie->end();it++){
        if(it->getx()==(this->getx()-1)&&((it->gety()==(this->gety()-1))||(it->gety()==this->gety())||(it->gety()==(this->gety()+1)))){
                                          this->atakuj(*it);
                                          cout<<(*it).hp<<endl;
                                          if((*it).gethp()<0){
                                          grid.at((it->getx()+100*it->gety())).leave();
                                          it->dropeq(przedmioty,orb);
                                          wrogowie->erase(it);
                                          }
            };
            if(wrogowie->empty())
                break;
        }
        break;
    case 3:
        for(vector<Obiekt>::iterator it=wrogowie->begin();it!=wrogowie->end();it++){
        if(it->gety()==(this->gety()+1)&&((it->getx()==(this->getx()-1))||(it->getx()==this->getx())||(it->getx()==(this->gety()+1)))){
                                          this->atakuj(*it);
                                          cout<<(*it).hp<<endl;
                                          if((*it).gethp()<0){
                                          grid.at((it->getx()+100*it->gety())).leave();
                                          it->dropeq(przedmioty,orb);
                                          wrogowie->erase(it);
                                          }
                };
                if(wrogowie->empty())
                break;
            }
            break;
            case 4:
                for(vector<Obiekt>::iterator it=wrogowie->begin();it!=wrogowie->end();it++){
                    if(it->gety()==(this->gety()-1)&&((it->getx()==(this->getx()-1))||(it->getx()==this->getx())||(it->getx()==(this->getx()+1)))){
                        this->atakuj(*it);
                        cout<<(*it).hp<<endl;
                        if((*it).gethp()<0){
                        grid.at((it->getx()+100*it->gety())).leave();
                        it->dropeq(przedmioty,orb);
                        wrogowie->erase(it);
                        }
                    };
                    if(wrogowie->empty())
                break;
                }
            break;
        }
    }

    void ruch(int kierunek,vector<Tile> &grid,vector<Obiekt> *obiekty){
        int koord;
        if(tik>szybkosc){
        switch (kierunek){
            case 1:
                koord=this->x+1+100*this->y;
                if(grid.at(koord).isempty()){
                grid.at(this->x+100*this->y).leave();
                grid.at(koord).occupy();
                this->x=this->x+1;
                }
                else if(!grid.at(koord).iswall()){
                    for(vector<Obiekt>::iterator it=obiekty->begin();it!=obiekty->end();it++){
                        if(koord==((*it).getx()+100*(*it).gety())){
                            this->kontaktdmg(*it);
                            cout<<(*it).hp<<endl;
                            if((*it).gethp()<0){
                                grid.at(koord).leave();
                                obiekty->erase(it);

                            }
                        };
                        if(obiekty->empty())
                        break;
                    }
                }
                break;
            case 2:
                koord=this->x+100*(this->y+1);
                if(grid.at(koord).isempty()){
                grid.at(this->x+100*this->y).leave();
                grid.at(koord).occupy();
                this->y=this->y+1;
                }
                else if(!grid.at(koord).iswall()){
                    for(vector<Obiekt>::iterator it=obiekty->begin();it!=obiekty->end();it++){
                        if(koord==((*it).getx()+100*(*it).gety())){
                            this->atakuj((*it));
                            cout<<(*it).hp<<endl;
                            if((*it).gethp()<0){
                                grid.at(koord).leave();
                                obiekty->erase(it);

                            }
                        };
                        if(obiekty->empty())
                        break;
                    }
                }
                break;
            case 3:
                koord=this->x-1+100*(this->y);
                if(grid.at(koord).isempty()){
                grid.at(this->x+100*this->y).leave();
                grid.at(koord).occupy();
                this->x=this->x-1;
                }
                else if(!grid.at(koord).iswall()){
                    for(vector<Obiekt>::iterator it=obiekty->begin();it!=obiekty->end();it++){
                        if(koord==((*it).getx()+100*(*it).gety())){
                            this->atakuj((*it));
                            cout<<(*it).hp<<endl;
                            if((*it).gethp()<0){
                                grid.at(koord).leave();
                                obiekty->erase(it);

                            }
                        };
                        if(obiekty->empty())
                        break;
                    }
                }
                break;
            case 4:
                koord=this->x+100*(this->y-1);
                if(grid.at(koord).isempty()){
                grid.at(this->x+100*this->y).leave();
                grid.at(koord).occupy();
                this->y=this->y-1;
                }
                else if(!grid.at(koord).iswall()){
                    for(vector<Obiekt>::iterator it=obiekty->begin();it!=obiekty->end();it++){
                        if(koord==((*it).getx()+100*(*it).gety())){
                            this->atakuj((*it));
                            cout<<(*it).hp<<endl;
                            if((*it).gethp()<0){
                                grid.at(koord).leave();
                                obiekty->erase(it);

                            }

                        };
                        if(obiekty->empty())
                        break;
                    }
                }
                break;
        }
        this->tik=this->tik-1;
        }
        else{
            this->tik=this->tik-1;
        }
        if(this->tik<0)
            this->tik=this->szybkosc+1;
    }

    void ruch(int kierunek,vector<Tile> &grid,vector<Obiekt*> *obiekty){
        int koord;
        if(tik>szybkosc){
        switch (kierunek){
            case 1:
                koord=this->x+1+100*this->y;
                if(grid.at(koord).isempty()){
                grid.at(this->x+100*this->y).leave();
                grid.at(koord).occupy();
                this->x=this->x+1;
                }
                else if(!grid.at(koord).iswall()){
                    for(vector<Obiekt*>::iterator it=obiekty->begin();it!=obiekty->end();it++){
                        if(koord==((**it).getx()+100*(**it).gety())){
                            this->kontaktdmg(**it);
                            cout<<(**it).hp<<endl;
                            if((**it).gethp()<0){
                                grid.at(koord).leave();
                                obiekty->erase(it);

                            }
                        };
                        if(obiekty->empty())
                        break;
                    }
                }
                break;
            case 2:
                koord=this->x+100*(this->y+1);
                if(grid.at(koord).isempty()){
                grid.at(this->x+100*this->y).leave();
                grid.at(koord).occupy();
                this->y=this->y+1;
                }
                else if(!grid.at(koord).iswall()){
                    for(vector<Obiekt*>::iterator it=obiekty->begin();it!=obiekty->end();it++){
                        if(koord==((**it).getx()+100*(**it).gety())){
                            this->atakuj((**it));
                            cout<<(**it).hp<<endl;
                            if((**it).gethp()<0){
                                grid.at(koord).leave();
                                obiekty->erase(it);

                            }
                        };
                        if(obiekty->empty())
                        break;
                    }
                }
                break;
            case 3:
                koord=this->x-1+100*(this->y);
                if(grid.at(koord).isempty()){
                grid.at(this->x+100*this->y).leave();
                grid.at(koord).occupy();
                this->x=this->x-1;
                }
                else if(!grid.at(koord).iswall()){
                    for(vector<Obiekt*>::iterator it=obiekty->begin();it!=obiekty->end();it++){
                        if(koord==((**it).getx()+100*(**it).gety())){
                            this->atakuj((**it));
                            cout<<(**it).hp<<endl;
                            if((**it).gethp()<0){
                                grid.at(koord).leave();
                                obiekty->erase(it);

                            }
                        };
                        if(obiekty->empty())
                        break;
                    }
                }
                break;
            case 4:
                koord=this->x+100*(this->y-1);
                if(grid.at(koord).isempty()){
                grid.at(this->x+100*this->y).leave();
                grid.at(koord).occupy();
                this->y=this->y-1;
                }
                else if(!grid.at(koord).iswall()){
                    for(vector<Obiekt*>::iterator it=obiekty->begin();it!=obiekty->end();it++){
                        if(koord==((**it).getx()+100*(**it).gety())){
                            this->atakuj((**it));
                            cout<<(**it).hp<<endl;
                            if((**it).gethp()<0){
                                grid.at(koord).leave();
                                obiekty->erase(it);

                            }

                        };
                        if(obiekty->empty())
                        break;
                    }
                }
                break;
        }
        this->tik=this->tik-1;
        }
        else{
            this->tik=this->tik-1;
        }
        if(this->tik<0)
            this->tik=this->szybkosc+1;
    }

    void getitem(vector<Przedmiot> &przedmioty){
    for(vector<Przedmiot>::iterator it=przedmioty.begin();it!=przedmioty.end();it++)
    if(this->x==it->getx()&&this->y==it->gety()){
        this->eq.push_back((*it));
        maxhp=it->getmaxhp()+maxhp;
        if((it->gethp()+hp)<=maxhp)
            hp=it->gethp()+hp;
        else
            hp=maxhp;
        atak=it->getatak()+atak;
        unik=it->getunik()+unik;
        obrona=it->getobrona()+obrona;
        szybkosc=szybkosc-it->getszybkosc();

        przedmioty.erase(it);
        if(przedmioty.empty())
        break;
    }

    }

    void giveitem(Przedmiot item){
    this->eq.push_back(item);
        maxhp=item.getmaxhp()+maxhp;
        hp=item.gethp()+hp;
        atak=item.getatak()+atak;
        unik=item.getunik()+unik;
        obrona=item.getobrona()+obrona;
        szybkosc=szybkosc-item.getszybkosc();
    }

    void showStat(){
        stringstream tmp;
        string tmp2,tmp3;
        tmp<<maxhp<<' '<<atak<<' '<<obrona<<' '<<unik<<' '<<' '<<szybkosc;
        tmp>>tmp2>>tmp3;
        tmp2="Statystyka\nhp:"+tmp2+"\natak:"+tmp3;
        tmp>>tmp3;
        tmp2=tmp2+"\nobrona:"+tmp3;
        tmp>>tmp3;
        tmp2=tmp2+"\nunik:"+tmp3;
        tmp>>tmp3;
        tmp2=tmp2+"\nszybkosc:"+tmp3;
        statystyka.setString(tmp2);
    }

    bool zapisz(ofstream *plik){
    plik->open("postacie_save.txt", ios::out | ios::app);
    if(plik->is_open()){
        (*plik)<<maxhp<<" "<<hp<<" "<<atak<<" "<<unik<<" "<<obrona<<" "<<szybkosc<<" "<<x<<" "<<y<<" ";
        (*plik)<<eq.size()<<" ";
        for(vector<Przedmiot>::iterator it=eq.begin();it!=eq.end();it++){
            (*plik)<<it->getmaxhp()<<" "<<it->gethp()<<" "<<it->getatak()<<" "<<it->getunik()<<" "<<it->getobrona()<<" "<<it->getszybkosc()<<" "<<it->getx()<<" "<<it->gety()<<" ";
        }
        plik->close();
        return true;
    }
    else
        plik->close();
        return false;
    }

    bool wczytaj(ifstream *plik,sf::Font font,sf::Texture *texture){
    font.loadFromFile("minimal.otf");
    if(plik->is_open()){
        if(!plik->eof()){
            int liczba_przedmiotow=0;
            (*plik)>>maxhp>>hp>>atak>>unik>>obrona>>szybkosc>>x>>y;
            (*plik)>>liczba_przedmiotow;
            for(;liczba_przedmiotow!=0;liczba_przedmiotow--){
                Przedmiot tmp(font,texture);
                tmp.wczytaj(plik,font);
                eq.push_back(tmp);
            }
            return true;
        }
        else{
            cout<<"\nkoniec\n";
            plik->close();
            return false;
        }
    }
    else{
        cout<<"\nblad otwarcia\n";
        return false;
    }
    }
};

void connect(Room room1,Room room2, vector<Tile> *grid){
		int cx1=((room2.x1+room2.x2)/2);
		int cy1=((room2.y1+room2.y2)/2);
		int cx2=((room1.x1+room1.x2)/2);
		int cy2=((room1.y1+room1.y2)/2);
		if(cx1<cx2){
			int tmp=cx1;
			cx1=cx2;
			cx2=tmp;
			};
		if(cy1<cy2){
			int tmp=cy1;
			cy1=cy2;
			cy2=tmp;
			};
		for(vector<Tile>::iterator it=(*grid).begin();it!=(*grid).end();it++){
		if(((*it).getx()<=cx1 && (*it).getx()>=cx2 )&&((*it).gety()==((room1.y1+room1.y2)/2))){
		(*it).leave();
		(*it).removewall();
		};
		if(((*it).gety()<=cy1 && (*it).gety()>=cy2 )&&((*it).getx()==((room2.x1+room2.x2)/2))){
		(*it).leave();
		(*it).removewall();
		};
		};

}

void initgrid(vector<Tile> *grid){
	for(int h=0;h<100;h++){
		for(int w=0;w<100;w++){
			Tile pole(w,h);
			pole.occupy();
			pole.setwall();
			(*grid).push_back(pole);
		}
	}
}

void placeRoom(vector<Tile> *grid,Room room){
	for(vector<Tile>::iterator it=(*grid).begin();it!=(*grid).end();it++){
		if((*it).getx()<room.x2 && (*it).getx()>room.x1 && (*it).gety()<room.y2 && (*it).gety()>room.y1){
		(*it).leave();
		(*it).removewall();
		}
}
}

bool zapisz(vector<Tile> grid,vector<Obiekt> wrogowie,Obiekt gracz,vector<Przedmiot> przedmioty){



ofstream plik;
plik.open("postacie_save.txt", ios::out | ios::trunc);
plik.close();
plik.open("przedmioty_save.txt", ios::out | ios::trunc);
plik.close();
plik.open("mapa_save.txt", ios::out | ios::trunc);
plik.close();


for(vector<Tile>::iterator it=grid.begin();it!=grid.end();it++){
    (*it).zapisz(&plik);
}

gracz.zapisz(&plik);

for(vector<Obiekt>::iterator it=wrogowie.begin();it!=wrogowie.end();it++){
    (*it).zapisz(&plik);
}

for(vector<Przedmiot>::iterator it=przedmioty.begin();it!=przedmioty.end();it++){
    (*it).zapisz(&plik);
}

return true;
}

bool wczytaj(vector<Tile> *grid,vector<Obiekt> *wrogowie,Obiekt *gracz,vector<Przedmiot> *przedmioty,sf::Font font,sf::Texture *postac,sf::Texture *mikstura){
ifstream plik;
plik.open("przedmioty_save.txt");
przedmioty->clear();
while(!plik.eof()){
    Przedmiot tmp(font,mikstura);
    tmp.wczytaj(&plik,font);
    przedmioty->push_back(tmp);
}
plik.close();
plik.open("postacie_save.txt");
gracz->wczytaj(&plik,font,mikstura);
wrogowie->clear();
while(!plik.eof()){
    Obiekt tmp(font,postac);
    tmp.wczytaj(&plik,font,mikstura);
    wrogowie->push_back(tmp);
}
plik.close();
grid->clear();
plik.open("mapa_save.txt");
while(!plik.eof()){
    Tile tmp(0,0);
    tmp.wczytaj(&plik);
    grid->push_back(tmp);
}
plik.close();
return true;
}

void displaygrid(vector<Tile> grid,vector<Obiekt> *wrogowie,Obiekt *gracz,vector<Przedmiot> *przedmioty,sf::Texture *mikstura,sf::Texture *postac){
int i=0;
sf::RenderWindow window(sf::VideoMode(1200, 800), "GAME RogueLike");
sf::Texture tile;
tile.loadFromFile("textures/tile.png",sf::IntRect(1,1,46,46));

sf::Texture bar;
bar.loadFromFile("textures/bar.png",sf::IntRect(0,0,100,2));
sf::Sprite healthbar(bar);
healthbar.setPosition(850,10);
gracz->tekstura.setPosition(400,400);
vector<Obiekt*> gracze;
        gracze.push_back(gracz);
sf::Font font;
font.loadFromFile("minimal.otf");
sf::Text tekst("HP\nW S A D RUCH\nSTRZALKI ATAK\nE PODNIES PRZEDMIOT\nO ZAPISZ\nP WCZYTAJ",font);
tekst.setPosition(850,0);
tekst.setStyle(sf::Text::Bold);
int kierunek=0;

while (window.isOpen())
    {
        if(!gracze.empty()&&!wrogowie->empty()&&!przedmioty->empty()){
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::KeyPressed){
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
                    kierunek=1;

                }
                else if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
                    kierunek=2;

                }
                else if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
                    kierunek=3;

                }
                else if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)){
                    kierunek=4;

                }
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::E)){
                    (*gracze.begin())->getitem(*przedmioty);
                };
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
                    (*gracze.begin())->atakowanie(1,grid,wrogowie,przedmioty,mikstura);
                   };
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
                    (*gracze.begin())->atakowanie(2,grid,wrogowie,przedmioty,mikstura);
                };
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
                    (*gracze.begin())->atakowanie(3,grid,wrogowie,przedmioty,mikstura);
                   };
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
                    (*gracze.begin())->atakowanie(4,grid,wrogowie,przedmioty,mikstura);
                };
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::O)){
                    zapisz(grid,*wrogowie,*gracz,*przedmioty);
                };
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::P)){
                    wczytaj(&grid,wrogowie,gracz,przedmioty,font,postac,mikstura);
                }
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)){
                    window.close();
                };
            }
            if (event.type == sf::Event::KeyReleased){
                kierunek=0;
            }

        }
        if(!gracze.empty()&&!wrogowie->empty())
            (*gracze.begin())->ruch(kierunek,grid,(wrogowie));
        if(!gracze.empty())
            (*gracze.begin())->showStat();
        if(!gracze.empty()&&!wrogowie->empty())
        for(vector<Obiekt>::iterator it=(*wrogowie).begin();it!=(*wrogowie).end();it++){
                        int gdzie=rand()%10;
                        if(((*it).getx()>=(*gracze.begin())->getx()-10)&&((*it).getx()<=(*gracze.begin())->getx()+10)&&((*it).gety()>=(*gracze.begin())->gety()-10)&&((*it).gety()<=(*gracze.begin())->gety()+10)){
                        if (gdzie==0){
                            if(((*it).getx()>(*gracze.begin())->getx()))
                                (*it).ruch(3,grid,&gracze);
                            if(((*it).getx()<(*gracze.begin())->getx()))
                                (*it).ruch(1,grid,&gracze);
                        }
                        else if(gdzie==1){
                            if(((*it).gety()>(*gracze.begin())->gety()))
                                (*it).ruch(4,grid,&gracze);
                            if(((*it).gety()<(*gracze.begin())->gety()))
                                (*it).ruch(2,grid,&gracze);
                        }
                        }
                    };
        window.clear();
        vector<sf::Sprite> mapa;
        vector<Tile>::iterator it=grid.begin();
        if(!gracze.empty()){
        advance(it,(((*gracze.begin())->getx()-10)+100*((*gracze.begin())->gety()-10)));
        vector<Tile>::iterator koniec=grid.begin();
        advance(koniec,(((*gracze.begin())->getx()+10)+100*((*gracze.begin())->gety()+10)));

        for(;it!=koniec;it++){
        if((it->getx()>=((*gracze.begin())->getx()-10))&&(it->getx()<=((*gracze.begin())->getx()+10))&&(it->gety()>=((*gracze.begin())->gety()-10))&&(it->gety()<=((*gracze.begin())->gety()+10))){
            double x,y;
            x=40*(it->getx()-(*gracze.begin())->getx()+10);
            y=40*(it->gety()-(*gracze.begin())->gety()+10);
            sf::Sprite kwadrat(tile);
            kwadrat.setTextureRect(sf::IntRect(0,0,45,45));

            kwadrat.setPosition(x,y);
            if((*it).isempty())
                kwadrat.setColor(sf::Color::Green);
            else
                kwadrat.setColor(sf::Color::White);
            ++i;
            mapa.push_back(kwadrat);
        }
        }
        }
        for(vector<sf::Sprite>::iterator it=mapa.begin();it!=mapa.end();it++)
		window.draw((*it));
		if(!wrogowie->empty()&&!gracze.empty())
		for(vector<Obiekt>::iterator it=(*wrogowie).begin();it!=(*wrogowie).end();it++){
            if(((*it).getx()>=(*gracze.begin())->getx()-10)&&((*it).getx()<=(*gracze.begin())->getx()+10)&&((*it).gety()>=(*gracze.begin())->gety()-10)&&((*it).gety()<=(*gracze.begin())->gety()+10)){
            double x,y;
            x=40*((*it).getx()-(*gracze.begin())->getx()+10);
            y=40*((*it).gety()-(*gracze.begin())->gety()+10);
            (*it).tekstura.setPosition(x,y);
            window.draw((*it).tekstura);
		}
		}
		if(!przedmioty->empty()&&!gracze.empty())
        for(vector<Przedmiot>::iterator it=(*przedmioty).begin();it!=(*przedmioty).end();it++){
            if(((*it).getx()>=(*gracze.begin())->getx()-10)&&((*it).getx()<=(*gracze.begin())->getx()+10)&&((*it).gety()>=(*gracze.begin())->gety()-10)&&((*it).gety()<=(*gracze.begin())->gety()+10)){
            double x,y;
            x=40*(it->getx()-(*gracze.begin())->getx()+10);
            y=40*(it->gety()-(*gracze.begin())->gety()+10);
            it->tekstura.setPosition(x,y);
            window.draw(it->tekstura);
            if(((*it).getx()==(*gracze.begin())->getx())&&((*it).gety()==(*gracze.begin())->gety())){
            it->nazwa.setFont(font);
            window.draw(it->nazwa);
            }
            }
        }
        if(!gracze.empty()){
        window.draw((*gracze.begin())->tekstura);
        (*gracze.begin())->statystyka.setFont(font);
        window.draw((*gracze.begin())->statystyka);
        healthbar.setScale(2*((*gracze.begin())->gethp()/(*gracze.begin())->getmaxhp()),4);
        window.draw(healthbar);
        }
        else{
            cout<<"GAME OVER"<<endl;
            };
        if(wrogowie->empty()){
            cout<<"WIN"<<endl;
        }
        window.draw(tekst);
        window.display();
    }
    else{
    sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        window.clear();
    if(gracze.empty()){
        sf::Text przegrana("KONIEC GRY",font);
        window.draw(przegrana);
        window.display();
    };
    if(wrogowie->empty()||przedmioty->empty()){
        sf::Text wygrana("KONIEC GRY!\n WYGRALES",font);
        window.draw(wygrana);
        window.display();
    }
    }
    }

}

int init_game(void){
	int maxw=100;
	int maxh=100;
	vector<Tile> grid;
	initgrid(&grid);
	srand(time(0));
	int seed=rand();
  srand(seed);
  sf::Font font;
  font.loadFromFile("minimal.otf");
	vector<Room> roomcont;
	Room a(maxw,maxh,&seed);
	roomcont.push_back(a);
	for (vector<Room>::iterator i=roomcont.begin();roomcont.size()<7;++i){
		Room b(maxw,maxh,&seed);
		int f=0;
		for(vector<Room>::iterator it=roomcont.begin();it!=roomcont.end();++it){
			if(b.intersect(*it)){
				f=1;
				};
		}
		if(f==0){
			roomcont.push_back(b);
		};
	};

	for(vector<Room>::iterator it=roomcont.begin();it!=roomcont.end();++it){
		placeRoom(&grid,(*it));
		if(it!=(roomcont.end()-1)){
			connect((*(it+1)),(*it),&grid);
		};
	};
	sf::Texture postac;
	sf::Texture mikstura;
	postac.loadFromFile("textures/character.png",sf::IntRect(1,1,10,10));
	mikstura.loadFromFile("textures/orb.png",sf::IntRect(0,0,10,10));
	Obiekt gracz(grid,roomcont.at(1),&seed,&postac,font);
	gracz.tekstura.setColor(sf::Color::Red);
	vector<Przedmiot> przedmioty;
	vector<Obiekt> wrogowie;
	for(vector<Room>::iterator it=roomcont.begin();it!=roomcont.end();it++)
		for(int i=0;i<2;i++){
    	Obiekt wrog(grid,*it,&seed,&postac,font);
    	Przedmiot przedmiot(grid,*it,&seed,font,&mikstura);
   	  przedmiot.tekstura.setTexture(mikstura);
    	przedmiot.tekstura.setScale(4,4);
    	wrog.giveitem(przedmiot);
    	wrogowie.push_back(wrog);
	};
	for(vector<Room>::iterator it=roomcont.begin();it!=roomcont.end();it++){
		for(int i=0;i<1;i++){
    	Przedmiot przedmiot(grid,*it,&seed,font,&mikstura);
    	przedmiot.tekstura.setTexture(mikstura);
    	przedmiot.tekstura.setScale(4,4);
    	przedmioty.push_back(przedmiot);
		}
	};

	displaygrid(grid,&wrogowie,&gracz,&przedmioty,&mikstura,&postac);
	return 0;
}

