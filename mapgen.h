#ifndef MAPGEN
	#define MAPGEN
	#include <iostream>
	#include <fstream>
	#include <sstream>
	#include <ctime>
	#include <vector>
	#include <cstdlib>
	#include <SFML/Graphics.hpp>
	#include <string>
	#include <SFML/Audio.hpp>

#ifdef linux
	#include "objects/button.hpp"
	#include "objects/textbox.hpp"
#endif

#ifdef _WIN32
	#include "objects\button.hpp"
	#include "objects\textbox.hpp"
#endif




	using namespace std;
	using namespace sf;
	class Tile{
		private:
			bool empty, wall;
			int x,y;
		public:
			Tile(int x,int y);
			void occupy();
			void leave();
			void setwall();
			void removewall();
			bool iswall();
			bool isempty();
			int getx();
			int gety();		
			bool zapisz(ofstream *plik);
			bool wczytaj(ifstream *plik);
	};
	
	int init_game(void);
#endif
